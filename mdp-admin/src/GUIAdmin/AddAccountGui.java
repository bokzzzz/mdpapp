/*
 * Created by JFormDesigner on Thu Jun 06 14:01:41 CEST 2019
 */

package GUIAdmin;

import RESTControler.PropClass;
import SOAPControler.Controler;
import SOAPControler.User;

import java.awt.*;
import java.awt.event.*;
import java.rmi.RemoteException;
import java.util.logging.Level;
import javax.swing.*;

/**
 * @author Bojan PetruŠić
 */
public class AddAccountGui extends JFrame {
    public AddAccountGui() {
        initComponents();
    }

    private void addUser(MouseEvent e) {
        String username=usernameField.getText();
        String password=new String(passwordField.getPassword());
        String repeatPassword=new String(repeatPasswordField.getPassword());
        if(!(username == null && repeatPassword.equals("") && password.equals("")) ){
            if(password.equals(repeatPassword)){
                try {
                    if(Controler.newAcc(new User(password,username))){
                        JOptionPane.showMessageDialog(null,"Add user successfully","INFO", 1);
                    }else
                        JOptionPane.showMessageDialog(null,"User not added","ERROR", 0);
                    setVisible(false);
                } catch (RemoteException ex) {
                    PropClass.log(Level.SEVERE, ex.getMessage());
                }
                usernameField.setText("");
                passwordField.setText("");
                repeatPasswordField.setText("");
            }
            else
                JOptionPane.showMessageDialog(null,"Incorrect user or password","ERROR", 0);
        }

    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Bojan PetruŠić
        label1 = new JLabel();
        label2 = new JLabel();
        label3 = new JLabel();
        usernameField = new JTextField();
        AddButton = new JButton();
        passwordField = new JPasswordField();
        repeatPasswordField = new JPasswordField();

        //======== this ========
        var contentPane = getContentPane();
        contentPane.setLayout(null);

        //---- label1 ----
        label1.setText("Username:");
        label1.setFont(label1.getFont().deriveFont(label1.getFont().getSize() + 6f));
        contentPane.add(label1);
        label1.setBounds(15, 55, 120, 30);

        //---- label2 ----
        label2.setText("Password:");
        label2.setFont(label2.getFont().deriveFont(label2.getFont().getSize() + 6f));
        contentPane.add(label2);
        label2.setBounds(15, 100, 140, 30);

        //---- label3 ----
        label3.setText("Repeat password:");
        label3.setFont(label3.getFont().deriveFont(label3.getFont().getSize() + 6f));
        contentPane.add(label3);
        label3.setBounds(15, 150, 165, 30);
        contentPane.add(usernameField);
        usernameField.setBounds(185, 55, 190, 30);

        //---- AddButton ----
        AddButton.setText("Add User");
        AddButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                addUser(e);
            }
        });
        contentPane.add(AddButton);
        AddButton.setBounds(180, 220, 195, 30);
        contentPane.add(passwordField);
        passwordField.setBounds(185, 105, 190, 30);
        contentPane.add(repeatPasswordField);
        repeatPasswordField.setBounds(185, 155, 190, 30);

        contentPane.setPreferredSize(new Dimension(435, 340));
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Bojan PetruŠić
    private JLabel label1;
    private JLabel label2;
    private JLabel label3;
    private JTextField usernameField;
    private JButton AddButton;
    private JPasswordField passwordField;
    private JPasswordField repeatPasswordField;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
