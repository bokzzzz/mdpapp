/*
 * Created by JFormDesigner on Fri Jun 07 15:56:43 CEST 2019
 */

package GUIAdmin;

import WebMon.WebMonControler;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.*;

/**
 * @author Bojan PetruŠić
 */
public class MonitoringGui extends JFrame {
    WebMonControler webCon=new WebMonControler();
    public MonitoringGui(String username) {
        initComponents();
        setResizable(false);
        Dimension d=getToolkit().getScreenSize();
        int widthScr=d.width;
        int heightScr=d.height;
        label1.setSize(widthScr,heightScr);
        webCon.start(username,label1,widthScr,heightScr);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Bojan PetruŠić
        label1 = new JLabel();

        //======== this ========
        setAlwaysOnTop(true);
        var contentPane = getContentPane();
        contentPane.setLayout(null);
        contentPane.add(label1);
        label1.setBounds(0, 0, 320, 185);

        contentPane.setPreferredSize(new Dimension(550, 400));
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        label1.setSize(getToolkit().getScreenSize());
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                WebMonControler.isConnect=false;

            }
        });
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Bojan PetruŠić
    private JLabel label1;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
