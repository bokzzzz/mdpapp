/*
 * Created by JFormDesigner on Thu Jun 06 12:35:13 CEST 2019
 */

package GUIAdmin;

import RESTControler.PropClass;
import WebMon.WebMonControler;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.logging.Level;
import javax.swing.*;

/**
 * @author Bojan PetruŠić
 */
public class AdminGui extends JFrame {
    public static String ADDRESMULTICAST;
    public static int PORTMULTICAST;
    static {
        new PropClass();
        ADDRESMULTICAST=PropClass.ADDRESMULTICAST;
        PORTMULTICAST=PropClass.PORTMULTICAST;
        WebMonControler.hi();
    }
    public AdminGui() {
        initComponents();
        refresh(null);
    }
    public static void main(String[] args) {
        new AdminGui().setVisible(true);
    }

    private void usersStatistics(MouseEvent e) {
        new UserStatistics().setVisible(true);
    }

    private void blockUser(MouseEvent e) {
        new BlockUserGui().setVisible(true);
    }

    private void unblockUser(MouseEvent e) {
        new UnblockUserGui().setVisible(true);
    }
    private void sendAnnouncement(MouseEvent e) {
        String announcement = announcementArea.getText();
        if (announcement == null) {
            JOptionPane.showMessageDialog(this, "Your announcement is empty.", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        try {
            InetAddress address = InetAddress.getByName(ADDRESMULTICAST);
            byte[] message = announcementArea.getText().getBytes();
            DatagramSocket socket = new DatagramSocket();
            DatagramPacket packet = new DatagramPacket(message, message.length, address, PORTMULTICAST);
            socket.send(packet);
            socket.close();
            announcementArea.setText("");
        } catch (IOException ex) {
            PropClass.log(Level.SEVERE, ex.getMessage());
        }
    }

    private void monitoring(MouseEvent e) {
        String username=(String)usersList.getSelectedValue();
        if(username == null) return;
        new MonitoringGui(username).setVisible(true);

    }

    private void addUserEv(MouseEvent e) {
        new AddAccountGui().setVisible(true);
    }

    private void refresh(MouseEvent e) {
        try {
            String[] users=WebMonControler.getActiveUsers();
            if(users == null) {
                DefaultListModel model=new DefaultListModel();
                model.clear();
                usersList.setModel(model);
                return;
            }

            usersList.setListData(users);
        } catch (IOException ex) {
            PropClass.log(Level.SEVERE, ex.getMessage());
        }
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Bojan PetruŠić
        scrollPane1 = new JScrollPane();
        usersList = new JList();
        statisticsButton = new JButton();
        monitoringButton = new JButton();
        blockButton = new JButton();
        unblockButton = new JButton();
        scrollPane2 = new JScrollPane();
        announcementArea = new JTextArea();
        label1 = new JLabel();
        label2 = new JLabel();
        button4 = new JButton();
        button1 = new JButton();
        button2 = new JButton();

        //======== this ========
        var contentPane = getContentPane();
        contentPane.setLayout(null);

        //======== scrollPane1 ========
        {
            scrollPane1.setViewportView(usersList);
        }
        contentPane.add(scrollPane1);
        scrollPane1.setBounds(25, 40, 110, 220);

        //---- statisticsButton ----
        statisticsButton.setText("Users Statistics");
        statisticsButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                usersStatistics(e);
            }
        });
        contentPane.add(statisticsButton);
        statisticsButton.setBounds(40, 310, 120, 30);

        //---- monitoringButton ----
        monitoringButton.setText("Monitoring");
        monitoringButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                monitoring(e);
            }
        });
        contentPane.add(monitoringButton);
        monitoringButton.setBounds(210, 310, 120, 30);

        //---- blockButton ----
        blockButton.setText("Block User");
        blockButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                blockUser(e);
            }
        });
        contentPane.add(blockButton);
        blockButton.setBounds(40, 365, 120, 30);

        //---- unblockButton ----
        unblockButton.setText("Unblock User");
        unblockButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                unblockUser(e);
            }
        });
        contentPane.add(unblockButton);
        unblockButton.setBounds(210, 365, 120, 30);

        //======== scrollPane2 ========
        {
            scrollPane2.setViewportView(announcementArea);
        }
        contentPane.add(scrollPane2);
        scrollPane2.setBounds(225, 40, 265, 220);

        //---- label1 ----
        label1.setText("Active users:");
        label1.setFont(label1.getFont().deriveFont(label1.getFont().getSize() + 6f));
        contentPane.add(label1);
        label1.setBounds(25, 10, 110, 21);

        //---- label2 ----
        label2.setText("Announcement:");
        label2.setFont(label2.getFont().deriveFont(label2.getFont().getSize() + 6f));
        contentPane.add(label2);
        label2.setBounds(230, 15, 155, 21);

        //---- button4 ----
        button4.setText("Send");
        button4.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                sendAnnouncement(e);
            }
        });
        contentPane.add(button4);
        button4.setBounds(365, 270, 125, 30);

        //---- button1 ----
        button1.setText("Add user");
        button1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                addUserEv(e);
            }
        });
        contentPane.add(button1);
        button1.setBounds(380, 365, 120, 30);

        //---- button2 ----
        button2.setText("Refresh");
        button2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                refresh(e);
            }
        });
        contentPane.add(button2);
        button2.setBounds(25, 265, 110, button2.getPreferredSize().height);

        contentPane.setPreferredSize(new Dimension(610, 455));
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                WebMonControler.bye();
                System.exit(0);
            }
        });
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Bojan PetruŠić
    private JScrollPane scrollPane1;
    private JList usersList;
    private JButton statisticsButton;
    private JButton monitoringButton;
    private JButton blockButton;
    private JButton unblockButton;
    private JScrollPane scrollPane2;
    private JTextArea announcementArea;
    private JLabel label1;
    private JLabel label2;
    private JButton button4;
    private JButton button1;
    private JButton button2;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
