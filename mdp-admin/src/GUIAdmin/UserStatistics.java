package GUIAdmin;

import java.awt.event.*;
import RESTControler.Controler;
import model.User;

import java.awt.*;
import javax.swing.*;

public class UserStatistics extends JFrame {
    public UserStatistics() {
        initComponents();
        initListUsers();
    }

    private void getStatistics(MouseEvent e) {
        String username=(String)usersList.getSelectedValue();
        if(username != null)
        statisticsArea.setText(Controler.getStatistics(new User(username,null)));
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Bojan PetruŠić
        scrollPane1 = new JScrollPane();
        usersList = new JList();
        scrollPane2 = new JScrollPane();
        statisticsArea = new JTextArea();
        label1 = new JLabel();
        label2 = new JLabel();

        //======== this ========
        var contentPane = getContentPane();
        contentPane.setLayout(null);

        //======== scrollPane1 ========
        {

            //---- usersList ----
            usersList.setFont(usersList.getFont().deriveFont(usersList.getFont().getSize() + 6f));
            usersList.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    getStatistics(e);
                }
            });
            scrollPane1.setViewportView(usersList);
        }
        contentPane.add(scrollPane1);
        scrollPane1.setBounds(15, 30, 180, 425);

        //======== scrollPane2 ========
        {
            scrollPane2.setViewportView(statisticsArea);
        }
        contentPane.add(scrollPane2);
        scrollPane2.setBounds(265, 30, 655, 425);

        //---- label1 ----
        label1.setText("Users:");
        label1.setFont(label1.getFont().deriveFont(label1.getFont().getSize() + 6f));
        contentPane.add(label1);
        label1.setBounds(15, 0, 90, 30);

        //---- label2 ----
        label2.setText("Statistics:");
        label2.setFont(label2.getFont().deriveFont(label2.getFont().getSize() + 6f));
        contentPane.add(label2);
        label2.setBounds(265, 5, 90, 25);

        contentPane.setPreferredSize(new Dimension(950, 520));
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Bojan PetruŠić
    private JScrollPane scrollPane1;
    private JList usersList;
    private JScrollPane scrollPane2;
    private JTextArea statisticsArea;
    private JLabel label1;
    private JLabel label2;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
    private void initListUsers(){
        usersList.setListData(Controler.getAllUsers());
    }
}
