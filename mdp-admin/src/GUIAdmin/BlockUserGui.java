/*
 * Created by JFormDesigner on Thu Jun 06 13:20:34 CEST 2019
 */

package GUIAdmin;

import java.awt.event.*;
import RESTControler.Controler;
import SOAPControler.User;

import java.awt.*;
import java.rmi.RemoteException;
import javax.swing.*;

/**
 * @author Bojan PetruŠić
 */
public class BlockUserGui extends JFrame {
    public BlockUserGui() {
        initComponents();
        initListUsers();
    }

    private void blockUser(MouseEvent e) {
        String userName=(String)usersList.getSelectedValue();
        if(userName != null) {
            try {
                if(SOAPControler.Controler.blockUser(new User(null,userName)))
                JOptionPane.showMessageDialog(null,"Blocked user successfully","INFO", 1);
                else JOptionPane.showMessageDialog(null,"User not blocked","Error login", 0);
            } catch (RemoteException ex) {
                JOptionPane.showMessageDialog(null,"Service not work","Error login", 0);
            }
        }
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Bojan PetruŠić
        scrollPane1 = new JScrollPane();
        usersList = new JList();
        label1 = new JLabel();

        //======== this ========
        var contentPane = getContentPane();
        contentPane.setLayout(null);

        //======== scrollPane1 ========
        {

            //---- usersList ----
            usersList.setFont(usersList.getFont().deriveFont(usersList.getFont().getSize() + 8f));
            usersList.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    blockUser(e);
                }
            });
            scrollPane1.setViewportView(usersList);
        }
        contentPane.add(scrollPane1);
        scrollPane1.setBounds(40, 35, 210, 325);

        //---- label1 ----
        label1.setText("Users:");
        label1.setFont(label1.getFont().deriveFont(label1.getFont().getSize() + 6f));
        contentPane.add(label1);
        label1.setBounds(40, 0, 120, 30);

        contentPane.setPreferredSize(new Dimension(295, 410));
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Bojan PetruŠić
    private JScrollPane scrollPane1;
    private JList usersList;
    private JLabel label1;
    // JFormDesigner - End of variables declaration  //GEN-END:variables

    private void initListUsers(){
        usersList.setListData(Controler.getAllUsers());
    }

}
