/**
 * AccountManagementServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package SOAPControler;

public class AccountManagementServiceLocator extends org.apache.axis.client.Service implements SOAPControler.AccountManagementService {

    public AccountManagementServiceLocator() {
    }


    public AccountManagementServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public AccountManagementServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for AccountManagement
    private java.lang.String AccountManagement_address = "http://localhost:9000/mdp_soap/AccountManagement";

    public java.lang.String getAccountManagementAddress() {
        return AccountManagement_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String AccountManagementWSDDServiceName = "AccountManagement";

    public java.lang.String getAccountManagementWSDDServiceName() {
        return AccountManagementWSDDServiceName;
    }

    public void setAccountManagementWSDDServiceName(java.lang.String name) {
        AccountManagementWSDDServiceName = name;
    }

    public SOAPControler.AccountManagement_PortType getAccountManagement() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(AccountManagement_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getAccountManagement(endpoint);
    }

    public SOAPControler.AccountManagement_PortType getAccountManagement(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            SOAPControler.AccountManagementSoapBindingStub _stub = new SOAPControler.AccountManagementSoapBindingStub(portAddress, this);
            _stub.setPortName(getAccountManagementWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setAccountManagementEndpointAddress(java.lang.String address) {
        AccountManagement_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (SOAPControler.AccountManagement_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                SOAPControler.AccountManagementSoapBindingStub _stub = new SOAPControler.AccountManagementSoapBindingStub(new java.net.URL(AccountManagement_address), this);
                _stub.setPortName(getAccountManagementWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("AccountManagement".equals(inputPortName)) {
            return getAccountManagement();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://services", "AccountManagementService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://services", "AccountManagement"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("AccountManagement".equals(portName)) {
            setAccountManagementEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
