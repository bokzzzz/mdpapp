/**
 * AccountManagement_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package SOAPControler;

public interface AccountManagement_PortType extends java.rmi.Remote {
    public boolean unblockUser(SOAPControler.User user) throws java.rmi.RemoteException;
    public boolean newAcc(SOAPControler.User user) throws java.rmi.RemoteException;
    public boolean blockUser(SOAPControler.User user) throws java.rmi.RemoteException;
}
