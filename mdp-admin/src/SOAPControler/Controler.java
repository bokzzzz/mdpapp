package SOAPControler;

import RESTControler.PropClass;

import javax.xml.rpc.ServiceException;
import java.rmi.RemoteException;
import java.util.logging.Level;

public class Controler {
    private static AccountManagementServiceLocator locator=new AccountManagementServiceLocator();
    private static AccountManagement_PortType accMang;
    static{
        try {
            accMang=locator.getAccountManagement();
        } catch (ServiceException e) {
            PropClass.log(Level.SEVERE, e.getMessage());
        }
    }

    public static boolean blockUser(User user) throws RemoteException {
        return  accMang.blockUser(user);
    }
    public static boolean unblockUser(User user) throws RemoteException {
        return accMang.unblockUser(user);
    }
    public static boolean newAcc(User user) throws RemoteException {
        return accMang.newAcc(user);
    }
}
