package Monitoring;

import RESTControler.PropClass;

import java.io.*;
import java.net.Socket;
import java.util.Map;
import java.util.logging.Level;

public class ChatServerThread extends Thread {
    private BufferedReader in;
    private PrintWriter out;
    private Socket socket;
    private boolean isConnected=true;
    private String name;
    private String watchUser;
    public ChatServerThread(Socket socket) throws IOException {
        this.socket=socket;
        in=new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
        start();
    }
    public void run(){
        while(isConnected){
            try {
                String msg=in.readLine();
                if(msg.startsWith("HI$")){
                    hi(msg);
                }
                else if(msg.startsWith("MSG$")){
                    sendMessage(msg);
                }
                else if(msg.startsWith("ADMIN$")){
                    ServerMonitor.mapUsers.get("Admin").transportMessage(msg);
                }
                else if(msg.startsWith("USERS$")){
                    StringBuilder users=new StringBuilder();
                    String sender=msg.replace("USERS$","");
                    for(String user:ServerMonitor.mapUsers.keySet()){
                        if(!user.equals("Admin") && !user.equals(sender))
                            users.append(user+",");
                    }
                    if(users.length() !=0)
                        users.deleteCharAt(users.lastIndexOf(","));
                    ServerMonitor.mapUsers.get(sender).transportMessage("USERS$"+users.toString());

                }
                else if(msg.startsWith("BYE$")){
                    try {
                        bye(msg);
                    }catch (IOException e){
                        PropClass.log(Level.SEVERE, e.getMessage());
                    }

                }
                else if(msg.startsWith("HIADMIN$")){
                    hiAdmin(msg);
                }


            } catch (IOException e) {
                PropClass.log(Level.SEVERE, e.getMessage());
            }

        }
    }

    private void hi(String msg){
        String username=msg.replace("HI$","");
        System.out.println("Login: "+username);
        ServerMonitor.mapUsers.put(username,this);
    }
    private void hiAdmin(String msg){
        ServerMonitor.mapUsers.put("Admin",this);
        while(isConnected){
            try {
                String msgAdmin=in.readLine();
                if(msgAdmin.startsWith("MONITOR$")){
                    String username=msg.replace("MONITOR$","");
                    String usernameMonitor=msgAdmin.replace("MONITOR$","");
                    if(ServerMonitor.mapUsers.get(usernameMonitor)!= null)
                    ServerMonitor.mapUsers.get(usernameMonitor).transportMessage("MONITOR$");
                    else ServerMonitor.mapUsers.get("Admin").transportMessage("OFFLINE$"+usernameMonitor);
                }
                else if(msgAdmin.startsWith("USERS$")){
                    StringBuilder users=new StringBuilder();
                    for(String user:ServerMonitor.mapUsers.keySet()){
                        if(!user.equals("Admin"))
                        users.append(user+",");
                    }
                    if(users.length() !=0)
                    users.deleteCharAt(users.lastIndexOf(","));
                    ServerMonitor.mapUsers.get("Admin").transportMessage(users.toString());
                }
                else if(msgAdmin.startsWith("BYE$")){
                    isConnected=false;
                    ServerMonitor.mapUsers.remove("Admin");
                    socket.close();
                    out.close();
                    in.close();
                }
            } catch (IOException e) {
                PropClass.log(Level.SEVERE, e.getMessage());
            }
        }
    }
    private void bye(String msg) throws IOException {
        String username=msg.replace("BYE$","");
        System.out.println("logout: "+username);
        isConnected=false;
        ServerMonitor.mapUsers.remove(username);
        if(ServerMonitor.mapUsers.get("Admin")!=null)
            ServerMonitor.mapUsers.get("Admin").transportMessage("OFFLINE$"+username);
        socket.close();
        out.close();
        in.close();
    }
    private void sendMessage(String msg){
        String[] params=msg.replace("MSG$","").replace("$","#").split("#");
        String sender=params[0];
        String receiver=params[1];
        String message=params[2];
        if(receiver.equals("Group")){
            for(Map.Entry<String, ChatServerThread> entry :ServerMonitor.mapUsers.entrySet()) {
                String key = entry.getKey();
                ChatServerThread value = entry.getValue();
                if(!key.equals(sender) && !key.equals("Admin")){
                    value.transportMessage("MSG$"+key+"$"+"Group"+"$"+sender+": "+message);}
            }
        }else
        ServerMonitor.mapUsers.get(receiver).transportMessage("MSG$"+receiver+"$"+sender+"$"+sender+": "+message);
    }
    public void transportMessage(String msg){
            out.println(msg);
    }
}
