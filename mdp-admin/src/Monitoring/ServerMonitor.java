package Monitoring;

import RESTControler.PropClass;

import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;

public class ServerMonitor {
    static ConcurrentHashMap<String, ChatServerThread> mapUsers=new ConcurrentHashMap<>();

    private static final int WEBSERVERPORT;
    private static final String KEYSTORE;
    private static final String KEYSTOREPASSWORD;
    static
    {
        PropClass prop=new PropClass();
        WEBSERVERPORT=PropClass.WEBSERVERPORT;
        KEYSTORE=PropClass.KEYSTORE;
        KEYSTOREPASSWORD=PropClass.KEYSTOREPASSWORD;
    }

    public static void main(String[] args) throws IOException {

        System.setProperty("javax.net.ssl.keyStore", KEYSTORE);
        System.setProperty("javax.net.ssl.keyStorePassword", KEYSTOREPASSWORD);

        SSLServerSocketFactory ssf = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
        ServerSocket ss = null;
        try {
            ss = ssf.createServerSocket(WEBSERVERPORT);

        } catch (IOException e) {
            PropClass.log(Level.SEVERE, e.getMessage());
        }
        System.out.println("ready");
        while (true) {
            Socket s = ss.accept();
            ChatServerThread chatThread = new ChatServerThread(s);
        }
    }

}
