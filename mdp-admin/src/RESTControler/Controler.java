package RESTControler;

import model.User;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

public class Controler {
    private static int PORT;
    private static String ADDRESS;
    private static String REST;
    static private WebTarget target;

    static{
        new PropClass();
        PORT=PropClass.PORT;
        ADDRESS=PropClass.ADDRESS;
        REST=PropClass.REST;
        target = ClientBuilder.newClient().target("http://"+ADDRESS+":"+PORT+REST);
    }
    public static boolean login(User user){
        return target.path("/login")
                .request(MediaType.TEXT_PLAIN_TYPE)
                .post(Entity.entity(user,MediaType.APPLICATION_JSON),boolean.class);
    }
    public static boolean logout(User user){
        return target.path("/logout")
                .request(MediaType.TEXT_PLAIN_TYPE)
                .post(Entity.entity(user,MediaType.APPLICATION_JSON),boolean.class);
    }
    public static boolean changePassword(User user){
        return target.path("/changePassword")
                .request(MediaType.TEXT_PLAIN_TYPE)
                .post(Entity.entity(user,MediaType.APPLICATION_JSON),boolean.class);
    }
    public static String getStatistics(User user){
        return target.path("/statistics")
                .request(MediaType.TEXT_PLAIN_TYPE)
                .post(Entity.entity(user,MediaType.APPLICATION_JSON),String.class);
    }
    public static String[] getAllUsers(){
        return target.path("/allUsers")
                .request(MediaType.APPLICATION_JSON)
                .get(String[].class);
    }
}
