package WebMon;

import RESTControler.PropClass;
import org.imgscalr.Scalr;

import javax.imageio.ImageIO;
import javax.net.ssl.SSLSocketFactory;
import javax.swing.*;
import java.awt.image.BufferedImage;

import java.io.*;
import java.net.Socket;
import java.util.Base64;
import java.util.logging.Level;

public class WebMonControler {
    private static final String WEBSERVERADDRESS;
    private static final int WEBSERVERPORT;
    private static final String KEYSTORE;
    private static final String KEYSTOREPASSWORD;
    private static SSLSocketFactory sf;
    private static Socket socket;
    private static PrintWriter out;
    private static BufferedReader in;
    public static boolean isConnect=true;

    static {
        new PropClass();
        WEBSERVERADDRESS=PropClass.WEBSERVERADDRESS;
        WEBSERVERPORT=PropClass.WEBSERVERPORT;
        KEYSTORE=PropClass.KEYSTORE;
        KEYSTOREPASSWORD=PropClass.KEYSTOREPASSWORD;
        System.setProperty("javax.net.ssl.trustStore", KEYSTORE);
        System.setProperty("javax.net.ssl.keyStorePassword", KEYSTOREPASSWORD);
        sf = (SSLSocketFactory) SSLSocketFactory.getDefault();

        try {
            socket = sf.createSocket(WEBSERVERADDRESS, WEBSERVERPORT);
            out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            PropClass.log(Level.SEVERE, e.getMessage());
        }

    }
    public  static  void hi(){
        out.println("HIADMIN$");
    }
    public static void start(String username, JLabel label,int targetWidth, int targetHeight) {
        isConnect=true;
        new Thread(()->{
            out.println("MONITOR$"+username);
        while(isConnect){
            try {

                String str=in.readLine();
                if(str.startsWith("OFFLINE$")) {
                    String offlineUsername=str.replace("OFFLINE$","");
                    if(offlineUsername.equals(username)) {
                        isConnect = false;
                        BufferedImage img = ImageIO.read(new File("offline.jpg"));
                        label.setIcon(new ImageIcon(Scalr.resize(img,Scalr.Mode.AUTOMATIC ,targetWidth,targetHeight)));
                    }
                }
                else if(str.startsWith("ADMIN$")){
                    String imageString=str.substring("ADMIN$".length());
                    byte[] imageByte = Base64.getDecoder().decode(imageString);
                    InputStream in = new ByteArrayInputStream(imageByte);
                    BufferedImage bImageFromConvert = ImageIO.read(in);
                    label.setIcon(new ImageIcon(Scalr.resize(bImageFromConvert,Scalr.Mode.AUTOMATIC ,targetWidth,targetHeight)));
                    out.println("MONITOR$"+username);
                }
            } catch (IOException e) {
                PropClass.log(Level.SEVERE, e.getMessage());
            }
        }
    }).start();}

    public static void bye(){
        out.println("BYE$Admin");
        try {
            socket.close();
        } catch (IOException e) {
            PropClass.log(Level.SEVERE, e.getMessage());
        }
        out.close();
    }
    public static String[] getActiveUsers() throws IOException {
        out.println("USERS$");
        String users=in.readLine();
        if(!users.startsWith("OFFLINE$") && !users.startsWith("ADMIN$"))
        if(users.length()!=0) return users.split(",");
        return null;
    }
}
