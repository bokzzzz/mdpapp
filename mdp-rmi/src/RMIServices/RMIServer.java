package RMIServices;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RMIServer {
    private static final int PORT;
    private static final String NAME;
    public static final String ROOTPATH;
    private static Logger logger = Logger.getLogger("error");
    static{
        Properties prop=new Properties();
        try {
            prop.load(new FileInputStream("rmi.properties"));
            logger.addHandler(new FileHandler("error.log", true));
            logger.setLevel(Level.INFO);

        } catch (IOException e) {
            e.printStackTrace();
        }
        NAME=prop.getProperty("NAME");
        PORT=Integer.parseInt(prop.getProperty("PORT"));
        ROOTPATH=prop.getProperty("ROOTPATH");

    }
    public static void main(String[] args) {
        System.setProperty("java.security.policy", "server.policy");
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        FileControl fcon = new FileControl();
        try {
            FileControlInterface fcint=(FileControlInterface) UnicastRemoteObject.exportObject(fcon,0);
            Registry reg= LocateRegistry.createRegistry(PORT);
            reg.rebind(NAME,fcint);
            System.out.println("Ready");
        } catch (RemoteException e) {
            log(Level.SEVERE, e.getMessage());
        }



    }
    public static void log(Level level, String message) {
        logger.log(level, message);
    }
}
