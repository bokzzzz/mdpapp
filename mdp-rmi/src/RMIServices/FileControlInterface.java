package RMIServices;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface FileControlInterface extends Remote {
    byte[] downloadFile(String fileName,String receiver,String sender) throws RemoteException;
    boolean uploadFile(String fileName,byte[] content,String sender,String receiver)throws  RemoteException;
    String[] listFile(String receiver ) throws RemoteException;

}
