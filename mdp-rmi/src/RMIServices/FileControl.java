package RMIServices;

import java.io.*;
import java.nio.file.Files;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.util.logging.Level;

public class FileControl implements FileControlInterface {
    @Override
    public byte[] downloadFile(String fileName,String receiver,String sender) throws RemoteException {
        File file=new File(RMIServer.ROOTPATH+ File.separator+receiver+ File.separator+ sender+"_"+fileName);
        try {
            byte[] content= Files.readAllBytes(file.toPath());
            return content;
        } catch (IOException e) {
            RMIServer.log(Level.SEVERE, e.getMessage());
        }
        return new byte[0];
    }

    @Override
    public boolean uploadFile(String fileName, byte[] content, String sender, String receiver) throws RemoteException {
        File directory=new File(RMIServer.ROOTPATH+File.separator+receiver);
        if(!directory.exists())
            directory.mkdir();


        File file=new File(RMIServer.ROOTPATH+File.separator + receiver+File.separator +sender+"_"+fileName);
        try {
            file.createNewFile();
        } catch (IOException e) {
            RMIServer.log(Level.SEVERE, e.getMessage());
           return false;
        }
        try (FileOutputStream out = new FileOutputStream(file)) {
            out.write(content);
        } catch (IOException e) {
            RMIServer.log(Level.SEVERE, e.getMessage());
            return false;
        }
        return true;

    }

    @Override
    public String[] listFile(String receiver) throws RemoteException {
        File directory = new File(RMIServer.ROOTPATH+File.separator+receiver);
        if(directory.isDirectory())
        return directory.list();
        return null;
    }
}
