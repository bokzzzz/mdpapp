package services;


import model.PropClass;
import model.User;
import redis.clients.jedis.Jedis;



public class AccountManagement {
        public static final String HOST;
        static{
                new PropClass();
                HOST=PropClass.HOST;
                System.out.println(HOST);
        }
        public boolean newAcc(User user){

                Jedis db=new Jedis(HOST);
                boolean isExistsUser=db.exists(user.getUsername());
                if(isExistsUser==false){
                        db.rpush(user.getUsername(),user.getPassword(),"aktivan");
                        return true;
                }
            return false;
        }
        public static boolean blockUser(User user){
                Jedis db=new Jedis(HOST);
                boolean isExistsUser=db.exists(user.getUsername());
                if(isExistsUser==true){
                        db.lset(user.getUsername(),1,"blokiran");
                        return true;
                }
                return false;
        }
        public static boolean unblockUser(User user){

                Jedis db=new Jedis(HOST);
                boolean isExistsUser=db.exists(user.getUsername());
                if(isExistsUser==true){
                        db.lset(user.getUsername(),1,"aktivan");
                        return true;
                }
                return false;
        }
}
