package RMIClient;

import RESTClient.PropClass;
import RMIServices.FileControlInterface;
import model.User;

import java.io.*;
import java.nio.file.Files;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Arrays;
import java.util.Properties;
import java.util.logging.Level;

public class FileControlClient {
    public static Registry registry;
    private static  int PORT;
    private static  String NAME;
    private static String RMIADDRESS;
    private static FileControlInterface server;
    static {
        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream("rmi.properties"));

        } catch (IOException e) {
            PropClass.log(Level.SEVERE, e.getMessage());
        }
        NAME = prop.getProperty("NAME");
        PORT = Integer.parseInt(prop.getProperty("PORT"));
        RMIADDRESS=prop.getProperty("RMIADDRESS");
        System.setProperty("java.security.policy", "server.policy");
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        try {
            registry = LocateRegistry.getRegistry(RMIADDRESS,PORT);
            server = (FileControlInterface) registry.lookup(NAME);
        } catch (RemoteException | NotBoundException e) {
            PropClass.log(Level.SEVERE, e.getMessage());
        }
    }
    public static String[] getFiles(User user) throws RemoteException {
        String files[]=server.listFile(user.getUsername());
        if(files==null) return null;
        return Arrays.asList(files).parallelStream().map(
                e -> {String[] params=e.split("_",2);
                return "Sender: "+params[0]+", file name: "+params[1];
        }).toArray(String[]::new);
    }
    public static boolean uploadFile(File file,String sender,String receiver) throws IOException {
        byte[] content= Files.readAllBytes(file.toPath());
        server.uploadFile(file.getName(),content,sender,receiver);
        return true;
    }
    public static boolean downloadFile(String fileName,String savePath,String sender,String receiver) throws IOException {
        byte[] content=server.downloadFile(fileName,receiver,sender);
        File file=new File(savePath);
        file.createNewFile();
        FileOutputStream out=new FileOutputStream(file);
        out.write(content);
        return true;
    }
    }
