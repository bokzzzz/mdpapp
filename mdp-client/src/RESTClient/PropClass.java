package RESTClient;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PropClass {
    public static String ADDRESS;
    public static int PORT;
    public static String REST;
    public static String ADDRESMULTICAST;
    public static int PORTMULTICAST;
    public static String WEBSERVERADDRESS;
    public static int WEBSERVERPORT;
    public static  String KEYSTORE;
    public static  String KEYSTOREPASSWORD;
    private static Logger logger = Logger.getLogger("error");
    public PropClass(){
        Properties prop=new Properties();
        Properties propMulticast=new Properties();
        Properties webServer=new Properties();
        try {
            prop.load(new FileInputStream("rest.properties"));
            ADDRESS=prop.getProperty("ADDRESS");
            PORT=Integer.parseInt(prop.getProperty("PORT"));
            REST=prop.getProperty("REST");
            propMulticast.load(new FileInputStream("multicast.properties"));
            ADDRESMULTICAST=propMulticast.getProperty("ADDRESMULTICAST");
            PORTMULTICAST=Integer.parseInt(propMulticast.getProperty("PORTMULTICAST"));
            webServer.load(new FileInputStream("webserver.properties"));
            WEBSERVERADDRESS=webServer.getProperty("WEBSERVERADDRESS");
            WEBSERVERPORT=Integer.parseInt(webServer.getProperty("WEBSERVERPORT"));
            KEYSTORE=webServer.getProperty("KEYSTORE");
            KEYSTOREPASSWORD=webServer.getProperty("KEYSTOREPASSWORD");
            logger.addHandler(new FileHandler("error.log", true));
            logger.setLevel(Level.INFO);

        } catch (IOException e) {
            e.printStackTrace();
        }


    }
    public static void log(Level level, String message) {
        logger.log(level, message);
    }


}
