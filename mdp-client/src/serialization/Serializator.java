package serialization;

import RESTClient.PropClass;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Output;
import com.google.gson.Gson;


import java.io.*;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;

import org.jboss.netty.handler.codec.serialization.ObjectEncoderOutputStream;

public class Serializator {
    private static Properties serProp=new Properties();
    static {
        try {
            serProp.load(new FileInputStream("serialization.properties"));
        } catch (IOException e) {
            PropClass.log(Level.SEVERE, e.getMessage());
        }

    }
    private static  void gson(String announcement)  {
        Gson gson = new Gson();
        Message msg=new Message(announcement);
        String jsonString = gson.toJson(msg);
        try (FileWriter fileWriter = new FileWriter("Announcements" + File.separator + "Output" + System.currentTimeMillis() + ".json")) {
            fileWriter.write(jsonString);
        } catch (IOException e) {
            PropClass.log(Level.SEVERE, e.getMessage());
        }
    }
    private static void kryo(String announcement) {
        Kryo kryo = new Kryo();
        kryo.register(Message.class);
        try (FileOutputStream fileOut = new FileOutputStream("Announcements" + File.separator + "Output" + System.currentTimeMillis() + ".kryo")) {
            Output out = new Output(fileOut);
            Message msg = new Message(announcement);
            kryo.writeClassAndObject(out, msg);
            out.close();
        } catch (IOException e) {
            PropClass.log(Level.SEVERE, e.getMessage());
        }

    }
    private static void javaser(String announcement) {
        Message msg=new Message(announcement);
        try (ObjectOutputStream ob = new ObjectOutputStream(new FileOutputStream("Announcements" + File.separator + "Output" + System.currentTimeMillis() + ".ser"))) {
            ob.writeObject(msg);
        } catch (IOException e) {
            PropClass.log(Level.SEVERE, e.getMessage());
        }
    }
    public static void serialize(String announcement) throws IOException {
        AtomicLong number= new AtomicLong(loadNumber());
        if(number.get() %4 == 0) javaser(announcement);
        else if(number.get() %4 == 1) kryo(announcement);
        else if(number.get() %4 == 2) gson(announcement);
        else netty(announcement);
        setNumber(String.valueOf(number.incrementAndGet()));
    }
    private static void netty(String announcement){
            try (FileOutputStream fileOutputStream = new FileOutputStream("Announcements" + File.separator + "Output" + System.currentTimeMillis() + ".netty", true);
                 ObjectEncoderOutputStream encoderOutputStream = new ObjectEncoderOutputStream(fileOutputStream)) {
                encoderOutputStream.writeObject(announcement);
                encoderOutputStream.flush();
            } catch (IOException e) {
                PropClass.log(Level.SEVERE, e.getMessage());
        }




    }
    private static long loadNumber(){
        return Long.parseLong(serProp.getProperty("NUMBERFILES"));
    }
    private static void setNumber(String value){
        serProp.setProperty("NUMBERFILES",value);
    }
   public static void saveProperties(){
        try {
            serProp.store(new FileOutputStream("serialization.properties"), null);
        } catch (IOException e) {
            PropClass.log(Level.SEVERE, e.getMessage());
        }
    }
}
