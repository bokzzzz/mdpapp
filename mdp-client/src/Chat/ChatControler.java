package Chat;

import RESTClient.PropClass;
import model.User;

import javax.imageio.ImageIO;
import javax.net.ssl.SSLSocketFactory;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.Socket;
import java.util.Base64;
import java.util.logging.Level;


public class ChatControler {
    private static final String WEBSERVERADDRESS;
    private static final int WEBSERVERPORT;
    private static final String KEYSTORE;
    private static final String KEYSTOREPASSWORD;
    private  SSLSocketFactory sf;
    private  Socket socket;
    private  PrintWriter out;
    private  BufferedReader in;
    static {
        PropClass prop=new PropClass();
        WEBSERVERPORT=PropClass.WEBSERVERPORT;
        KEYSTORE=PropClass.KEYSTORE;
        KEYSTOREPASSWORD=PropClass.KEYSTOREPASSWORD;
        WEBSERVERADDRESS=PropClass.WEBSERVERADDRESS;
        System.setProperty("javax.net.ssl.trustStore", KEYSTORE);
        System.setProperty("javax.net.ssl.keyStorePassword", KEYSTOREPASSWORD);
    }
    {
        sf= (SSLSocketFactory) SSLSocketFactory.getDefault();

        try {
            socket = sf.createSocket(WEBSERVERADDRESS, WEBSERVERPORT);
            out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
            in=new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            PropClass.log(Level.SEVERE, e.getMessage());
        }

    }


    public  void hi(User user) {
        out.println("HI$"+user.getUsername());
    }
    public  void bye(User user) throws IOException {
        out.println("BYE$"+user.getUsername());
        socket.close();
        out.close();
    }
    public  void sendMessage(String message,User sender,User receiver){
        if(message.equals("")) return ;
        String send="MSG$"+sender.getUsername()+"$"+receiver.getUsername()+"$"+message;
        out.println(send);
    }
    public  String receiveMessage() throws IOException {
        String message=in.readLine();
        if(message == null) return "" ;
        if(message.startsWith("MSG$")) {
            String[] params = message.replace("MSG$", "").replace("$", "#").split("#");
            return "MSG$"+params[1] + ": " + params[2];
        }
        else if(message.startsWith("MONITOR$")) {
            out.println("ADMIN$"+sendPicture());
        }
        else if(message.startsWith("USERS$")){
            return message;
        }

        return "";
    }

    public String sendPicture(){
        Robot robot= null;
        try {
            robot = new Robot();
            Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
            BufferedImage screenFullImage = robot.createScreenCapture(screenRect);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ImageIO.write(screenFullImage, "jpg", out);
            byte[] imageBytes=out.toByteArray();
            String imageString= Base64.getEncoder().encodeToString(imageBytes);
            return imageString;
        } catch (Exception e) {
            PropClass.log(Level.SEVERE, e.getMessage());
        }


    return "";
    }
    public void getActiveUsers(String user) throws IOException {
        out.println("USERS$"+user);
    }
}


