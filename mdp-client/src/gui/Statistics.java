/*
 * Created by JFormDesigner on Wed Jun 05 14:45:30 CEST 2019
 */

package gui;

import RESTClient.RESTControler;
import model.User;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * @author Bojan PetruŠić
 */
public class Statistics extends JFrame {
    User user;
    public Statistics(User user) {
        initComponents();
        setResizable(false);
        this.user=user;
        getStatistics();

    }

    private void getStatistics() {
        statisticsArea.setText("");
        statisticsArea.setText(RESTControler.getStatistics(user));
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Bojan PetruŠić
        scrollPane1 = new JScrollPane();
        statisticsArea = new JTextArea();

        //======== this ========
        var contentPane = getContentPane();
        contentPane.setLayout(null);

        //======== scrollPane1 ========
        {
            scrollPane1.setViewportView(statisticsArea);
        }
        contentPane.add(scrollPane1);
        scrollPane1.setBounds(25, 15, 665, 440);

        contentPane.setPreferredSize(new Dimension(715, 505));
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Bojan PetruŠić
    private JScrollPane scrollPane1;
    private JTextArea statisticsArea;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
