package gui;

import Chat.ChatControler;
import RESTClient.PropClass;
import RESTClient.RESTControler;
import RMIClient.FileControlClient;
import model.User;
import serialization.Serializator;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.Collections;
import java.util.logging.Level;
import javax.swing.*;
import javax.swing.filechooser.FileSystemView;


public class GUIClient extends JFrame {
    private JFrame parrentJFrame;
    private User loginUser;
    private boolean loggedIn=true;
    private JTextArea areas[];
    private JScrollPane[] scrollPanes;
    private  int activeArea=0;
    ChatControler chat=new ChatControler();
    public GUIClient(JFrame parrent, User user) {
        initComponents();
        DefaultListModel model =new DefaultListModel();
        activeUsersList.setModel(model);
        setResizable(false);
        parrentJFrame=parrent;
        loginUser=user;
        initListUsers();
        initListFiles();
        receiveAnnouncement();
        try {
           chat.hi(loginUser);
        } catch (Exception e) {
            PropClass.log(Level.SEVERE, e.getMessage());
        }
        receviedMessageChat();

        refresh(null);
    }

    private void sendMessage(MouseEvent e) {
        String receiver=(String)activeUsersList.getSelectedValue();
        if(receiver == null) return;
        if(chatField.getText().equals("")) return;
        for(int i=0;i<usersList.getModel().getSize();i++) {
            if(usersList.getModel().getElementAt(i).equals(receiver)) {
                areas[i].append(loginUser.getUsername() + ": " + chatField.getText() + "\n");
                chat.sendMessage(chatField.getText(), loginUser, new User(receiver, null));
                chatField.setText("");
            }
        }
    }

    private void logout(MouseEvent e) {
        if(RESTControler.logout(loginUser)){
            setVisible(false);
            parrentJFrame.setVisible(true);
            loggedIn=false;
            try {
                chat.bye(loginUser);
            } catch (IOException ex) {
                PropClass.log(Level.SEVERE, ex.getMessage());
            }
        }
        else{
            JOptionPane.showMessageDialog(null,"Somethings is wrong!","Error", 0);
        }
    }

    private void changePassword(MouseEvent e) {
        new ChangePassword(loginUser).setVisible(true);
    }

    private void getStatistics(MouseEvent e) {
        new Statistics(loginUser).setVisible(true);
    }

    private void listFiles(MouseEvent e) {
        initListFiles();
    }

    private void uploadFile(MouseEvent e) {
        JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());

        int returnValue = jfc.showOpenDialog(null);

        if (returnValue == JFileChooser.APPROVE_OPTION) {
            File selectedFile = jfc.getSelectedFile();
            try {
                String receiver =(String)usersList.getSelectedValue();
                if(receiver != null) {
                    FileControlClient.uploadFile(selectedFile, loginUser.getUsername(), receiver);
                    JOptionPane.showMessageDialog(null, "File successfully uploaded!", "Info", 1);
                }
                else JOptionPane.showMessageDialog(null, "User not choosed!", "Info", 1);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null,"File not uploaded","Error", 0);
            }
        }
        else JOptionPane.showMessageDialog(null,"File not choosed","Error", 0);
    }

    private void downloadFile(MouseEvent e) {
        String rawFileString =(String)fileList.getSelectedValue();
        if(rawFileString != null){
            String[] params=rawFileString.split(",");
            String sender=params[0].replace("Sender: ","");
            String nameFile=params[1].replace(" file name: ","");

            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("Specify a file to save");

            int userSelection = fileChooser.showSaveDialog(null);

            if (userSelection == JFileChooser.APPROVE_OPTION) {
                File fileToSave = fileChooser.getSelectedFile();
                try {
                    FileControlClient.downloadFile(nameFile,fileToSave.getAbsolutePath(),sender,loginUser.getUsername());
                    JOptionPane.showMessageDialog(null,"File successfully downloaded","INFO", 1);
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(null,"File not downloaded","Error", 0);
                }
            }
        }else{
            JOptionPane.showMessageDialog(null,"File not dowloaded","Error", 0);
        }
    }

    private void showChat(MouseEvent e) {
        int index=usersList.getSelectedIndex();
        areas[activeArea].setVisible(false);
        scrollPanes[activeArea].setVisible(false);
        areas[index].setVisible(true);
        scrollPanes[index].setVisible(true);
        activeArea=index;
    }

    private void refresh(MouseEvent e) {
        try {
            chat.getActiveUsers(loginUser.getUsername());
        } catch (IOException ex) {
            PropClass.log(Level.SEVERE, ex.getMessage());
        }
    }

    private void activeUsersPress(MouseEvent e) {
        if(activeUsersList.isSelectionEmpty()) return;
        String user=(String)activeUsersList.getSelectedValue();
        int indexUser=((DefaultListModel)usersList.getModel()).indexOf(user);
        areas[activeArea].setVisible(false);
        scrollPanes[activeArea].setVisible(false);
        areas[indexUser].setVisible(true);
        scrollPanes[indexUser].setVisible(true);
        activeArea=indexUser;
    }


    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Bojan PetruŠić
        scrollPane1 = new JScrollPane();
        usersList = new JList();
        label1 = new JLabel();
        label2 = new JLabel();
        label3 = new JLabel();
        scrollPane3 = new JScrollPane();
        fileList = new JList();
        DownloadButton = new JButton();
        uploadButton = new JButton();
        listButton = new JButton();
        chatField = new JTextField();
        sendButton = new JButton();
        label4 = new JLabel();
        button1 = new JButton();
        button2 = new JButton();
        button3 = new JButton();
        labelAnn = new JLabel();
        label5 = new JLabel();
        scrollPane2 = new JScrollPane();
        activeUsersList = new JList();
        button4 = new JButton();

        //======== this ========
        setTitle("Client APP");
        var contentPane = getContentPane();
        contentPane.setLayout(null);

        //======== scrollPane1 ========
        {

            //---- usersList ----
            usersList.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    showChat(e);
                }
            });
            scrollPane1.setViewportView(usersList);
        }
        contentPane.add(scrollPane1);
        scrollPane1.setBounds(25, 40, 125, 110);

        //---- label1 ----
        label1.setText("Users:");
        label1.setFont(label1.getFont().deriveFont(label1.getFont().getSize() + 6f));
        contentPane.add(label1);
        label1.setBounds(25, 15, 65, 25);

        //---- label2 ----
        label2.setText("Chat:");
        label2.setFont(label2.getFont().deriveFont(label2.getFont().getSize() + 6f));
        contentPane.add(label2);
        label2.setBounds(185, 15, 65, 25);

        //---- label3 ----
        label3.setText("List files:");
        label3.setFont(label3.getFont().deriveFont(label3.getFont().getSize() + 6f));
        contentPane.add(label3);
        label3.setBounds(435, 20, 80, 25);

        //======== scrollPane3 ========
        {
            scrollPane3.setViewportView(fileList);
        }
        contentPane.add(scrollPane3);
        scrollPane3.setBounds(430, 50, 240, 215);

        //---- DownloadButton ----
        DownloadButton.setText("Download File");
        DownloadButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                downloadFile(e);
            }
        });
        contentPane.add(DownloadButton);
        DownloadButton.setBounds(690, 50, DownloadButton.getPreferredSize().width, 30);

        //---- uploadButton ----
        uploadButton.setText("Upload File");
        uploadButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                uploadFile(e);
            }
        });
        contentPane.add(uploadButton);
        uploadButton.setBounds(690, 140, 105, 30);

        //---- listButton ----
        listButton.setText("List File");
        listButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                listFiles(e);
            }
        });
        contentPane.add(listButton);
        listButton.setBounds(690, 225, 105, 30);
        contentPane.add(chatField);
        chatField.setBounds(185, 295, 140, 30);

        //---- sendButton ----
        sendButton.setText("Send");
        sendButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                sendMessage(e);
            }
        });
        contentPane.add(sendButton);
        sendButton.setBounds(335, 295, 70, 30);

        //---- label4 ----
        label4.setText("Text:");
        label4.setFont(label4.getFont().deriveFont(label4.getFont().getSize() + 6f));
        contentPane.add(label4);
        label4.setBounds(185, 270, 70, 20);

        //---- button1 ----
        button1.setText("User Statistics");
        button1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                getStatistics(e);
            }
        });
        contentPane.add(button1);
        button1.setBounds(35, 360, 160, 30);

        //---- button2 ----
        button2.setText("Change Password");
        button2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                changePassword(e);
            }
        });
        contentPane.add(button2);
        button2.setBounds(255, 360, 160, 30);

        //---- button3 ----
        button3.setText("Logout");
        button3.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                logout(e);
            }
        });
        contentPane.add(button3);
        button3.setBounds(470, 360, 195, 30);

        //---- labelAnn ----
        labelAnn.setText("Announcement:");
        labelAnn.setFont(labelAnn.getFont().deriveFont(labelAnn.getFont().getSize() + 6f));
        contentPane.add(labelAnn);
        labelAnn.setBounds(35, 420, 625, 35);

        //---- label5 ----
        label5.setText("Active users:");
        label5.setFont(label5.getFont().deriveFont(label5.getFont().getSize() + 6f));
        contentPane.add(label5);
        label5.setBounds(25, 150, 125, 30);

        //======== scrollPane2 ========
        {

            //---- activeUsersList ----
            activeUsersList.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    activeUsersPress(e);
                }
            });
            scrollPane2.setViewportView(activeUsersList);
        }
        contentPane.add(scrollPane2);
        scrollPane2.setBounds(25, 180, 125, 110);

        //---- button4 ----
        button4.setText("Refresh");
        button4.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                refresh(e);
            }
        });
        contentPane.add(button4);
        button4.setBounds(25, 295, 125, 30);

        contentPane.setPreferredSize(new Dimension(855, 505));
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                Serializator.saveProperties();
                logout(null);
            }
        });
    }
    private void initListUsers(){
        String[] users=RESTControler.getAllUsers();
        if(users == null) return;
        DefaultListModel modelList=new DefaultListModel();
        //usersList.setListData(users);
        modelList.addElement("Group");
        modelList.addAll(Arrays.asList(users));
        modelList.removeElement(loginUser.getUsername());
        usersList.setModel(modelList);
        var contentPane = getContentPane();
        int lengthUsers=users.length;
        areas=new JTextArea[lengthUsers];
        scrollPanes=new JScrollPane[lengthUsers];
        for(int i=0;i<lengthUsers;i++){
                areas[i]=new JTextArea();
                areas[i].setVisible(false);
                areas[i].setEditable(false);
                scrollPanes[i]=new JScrollPane(areas[i]);
                scrollPanes[i].setViewportView(areas[i]);
                scrollPanes[i].setBounds(185,50,220,215);
                scrollPanes[i].setVisible(false);
                contentPane.add(scrollPanes[i]);

        }

    }
    private void initListFiles(){
        try {
            String[] files=FileControlClient.getFiles(loginUser);
            if(files == null) return;
            fileList.setListData(files);
        } catch (RemoteException e) {
            PropClass.log(Level.SEVERE, e.getMessage());
        }
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Bojan PetruŠić
    private JScrollPane scrollPane1;
    private JList usersList;
    private JLabel label1;
    private JLabel label2;
    private JLabel label3;
    private JScrollPane scrollPane3;
    private JList fileList;
    private JButton DownloadButton;
    private JButton uploadButton;
    private JButton listButton;
    private JTextField chatField;
    private JButton sendButton;
    private JLabel label4;
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JLabel labelAnn;
    private JLabel label5;
    private JScrollPane scrollPane2;
    private JList activeUsersList;
    private JButton button4;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
    public void receiveAnnouncement(){
        loggedIn=true;
        new Thread(()-> {
            MulticastSocket multicastSocket = null;
            try {
                multicastSocket = new MulticastSocket(PropClass.PORTMULTICAST);
                InetAddress address = InetAddress.getByName(PropClass.ADDRESMULTICAST);
                multicastSocket.joinGroup(address);
            } catch (IOException e) {
                PropClass.log(Level.SEVERE, e.getMessage());
            }
            while(loggedIn){
                try {
                    byte[] message = new byte[256];
                    DatagramPacket packet = new DatagramPacket(message, message.length);
                    multicastSocket.receive(packet);
                    labelAnn.setText("Announcement: " + new String(message));
                    Serializator.serialize(new String(message));
                    Thread.sleep(500);
                } catch ( InterruptedException | IOException e) {
                    PropClass.log(Level.SEVERE, e.getMessage());
                }
            }}).start();
    }
    public void receviedMessageChat(){
        new Thread(()->
        {
            while(loggedIn) {
                try {
                    String receiveMessage=chat.receiveMessage();
                    if(receiveMessage.startsWith("MSG$")){
                    String message=receiveMessage.replace("MSG$","")+"\n";
                    String[] params=message.split(":",2);
                    for(int i=0;i<usersList.getModel().getSize();i++) {
                        if (usersList.getModel().getElementAt(i).equals(params[0])) {
                            areas[i].append(params[1].substring(1));
                        }
                    }
                }else if(receiveMessage.startsWith("USERS$")){
                        String usersPars=receiveMessage.replace("USERS$","");
                        if(usersPars.equals("")) {
                            DefaultListModel model=((DefaultListModel)activeUsersList.getModel());
                            model.clear();
                            activeUsersList.setModel(model);
                        }
                        else{
                        String[] users=usersPars.split(",");
                        DefaultListModel model=((DefaultListModel)activeUsersList.getModel());
                        model.clear();
                        for(String user:users)
                            model.addElement(user);
                        model.addElement("Group");
                        activeUsersList.setModel(model);}
                    }
                }catch (IOException e) {
                    PropClass.log(Level.SEVERE, e.getMessage());
                }
            }
        }).start();
    }
}
