/*
 * Created by JFormDesigner on Wed Jun 05 12:08:54 CEST 2019
 */

package gui;

import RESTClient.RESTControler;
import model.User;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class LoginGui extends JFrame {
    public LoginGui() {
        initComponents();
        setResizable(false);
    }

    public static void main(String[] args) {
        new LoginGui().setVisible(true);
    }

    private void login(MouseEvent e) {
        User user=new User(usernameField.getText(),new String(passwordField.getPassword()));
        if(RESTControler.login(user)){
            usernameField.setText("");
            passwordField.setText("");
            setVisible(false);
            new GUIClient(this,user).setVisible(true);
        }
        else {
            JOptionPane.showMessageDialog(null,"Wrong username or password","Error login", 0);
        }
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Bojan PetruŠić
        label1 = new JLabel();
        label2 = new JLabel();
        usernameField = new JTextField();
        passwordField = new JPasswordField();
        button1 = new JButton();

        //======== this ========
        setTitle("Login Client");
        var contentPane = getContentPane();
        contentPane.setLayout(null);

        //---- label1 ----
        label1.setText("Username:");
        contentPane.add(label1);
        label1.setBounds(65, 110, 70, 30);

        //---- label2 ----
        label2.setText("Password:");
        contentPane.add(label2);
        label2.setBounds(65, 165, 70, 25);
        contentPane.add(usernameField);
        usernameField.setBounds(145, 110, 140, 30);
        contentPane.add(passwordField);
        passwordField.setBounds(145, 160, 140, 30);

        //---- button1 ----
        button1.setText("Login");
        button1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                login(e);
            }
        });
        contentPane.add(button1);
        button1.setBounds(150, 210, 130, button1.getPreferredSize().height);

        contentPane.setPreferredSize(new Dimension(325, 350));
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Bojan PetruŠić
    private JLabel label1;
    private JLabel label2;
    private JTextField usernameField;
    private JPasswordField passwordField;
    private JButton button1;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
