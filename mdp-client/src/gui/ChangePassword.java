/*
 * Created by JFormDesigner on Wed Jun 05 13:58:20 CEST 2019
 */

package gui;

import java.awt.event.*;

import RESTClient.RESTControler;
import model.User;

import java.awt.*;
import javax.swing.*;

/**
 * @author Bojan PetruŠić
 */
public class ChangePassword extends JDialog {
    User user;
    public ChangePassword(User user) {
        initComponents();
        this.user=user;
        setResizable(false);
        setModal(true);
    }

    private void changePassword(MouseEvent e) {
        if(new String(passwordField.getPassword()).equals(new String(repeatPasswordField.getPassword()))){
            if(RESTControler.changePassword(new User(user.getUsername(),new String(passwordField.getPassword())))){
                JOptionPane.showMessageDialog(null,"Change successfully!","Change password", JOptionPane.INFORMATION_MESSAGE);
                setVisible(false);
            }
            else
                JOptionPane.showMessageDialog(null,"Somethings is wrong!","Error", 0);
        }
        else{
            JOptionPane.showMessageDialog(null,"Password don't match!","Error", 0);
        }
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Bojan PetruŠić
        label1 = new JLabel();
        label2 = new JLabel();
        button2 = new JButton();
        passwordField = new JPasswordField();
        repeatPasswordField = new JPasswordField();

        //======== this ========
        var contentPane = getContentPane();
        contentPane.setLayout(null);

        //---- label1 ----
        label1.setText("Password:");
        contentPane.add(label1);
        label1.setBounds(15, 45, 90, 30);

        //---- label2 ----
        label2.setText("Repeat Password:");
        contentPane.add(label2);
        label2.setBounds(15, 100, 95, 30);

        //---- button2 ----
        button2.setText("Change");
        button2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                changePassword(e);
            }
        });
        contentPane.add(button2);
        button2.setBounds(85, 160, 105, 30);
        contentPane.add(passwordField);
        passwordField.setBounds(120, 45, 135, 30);
        contentPane.add(repeatPasswordField);
        repeatPasswordField.setBounds(120, 100, 135, 30);

        contentPane.setPreferredSize(new Dimension(275, 290));
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Bojan PetruŠić
    private JLabel label1;
    private JLabel label2;
    private JButton button2;
    private JPasswordField passwordField;
    private JPasswordField repeatPasswordField;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
