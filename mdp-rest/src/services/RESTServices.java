package services;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import Contoller.Controller;
import model.User;

@Path("/")
public class RESTServices {
    @POST
    @Path("login")
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    public boolean login(User user) {
        return Controller.checkCredentials(user);
    }
    @POST
    @Path("statistics")
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    public String getStatistics(User user)
    {
        return Controller.getStatistics(user);
    }
    @POST
    @Path("logout")
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    public boolean logout(User user){
        return Controller.logout(user);
    }
    @POST
    @Path("changePassword")
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    public boolean changePassword(User user){
        return Controller.changePassword(user);
    }
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("allUsers")
    public String[] getAllUsers(){
        return Controller.getAllUsers();
    }

}
