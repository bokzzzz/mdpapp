package Contoller;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PropClass {
    public static String HOST;
    private static Logger logger = Logger.getLogger("error");
    public PropClass(){
        Properties prop=new Properties();
        try {
            prop.load(getClass().getClassLoader().getResourceAsStream("../rest.properties"));
            HOST=prop.getProperty("HOST");
            logger.addHandler(new FileHandler("error.log", true));
            logger.setLevel(Level.INFO);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
    public static void log(Level level, String message) {
        logger.log(level, message);
    }
}
