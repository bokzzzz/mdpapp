package Contoller;

import model.User;
import redis.clients.jedis.Jedis;
import services.RESTServices;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;

public class Controller {
    private static final long SECONDSCONST= 1_000;
    public static final String HOST;
    static {
        new PropClass();
        HOST=PropClass.HOST;
        System.out.println(HOST);
    }

    public static boolean checkCredentials(User user){
        Jedis db = new Jedis(HOST);
        try {
            String password = db.lindex(user.getUsername(), 0);
            String activateStatus=db.lindex(user.getUsername(),1);
            if(activateStatus.equals("aktivan") && password.equals(user.getPassword())){
                db.rpush(user.getUsername(),Long.toString(System.currentTimeMillis()));
                db.close();
                return true;
            }
        }catch (Exception nl){
            PropClass.log(Level.SEVERE, nl.getMessage());
            return false;
        }
        return false;
    }
    public static boolean logout(User user)
    {
        try {
           Jedis db = new Jedis(HOST);
            String loginTime = db.rpop(user.getUsername());
            long loginTimeMilis = Long.parseLong(loginTime);
            long logoutTimeMilis = System.currentTimeMillis();
            long activateTime = (logoutTimeMilis - loginTimeMilis) / SECONDSCONST;
            db.rpush(user.getUsername(),Long.toString(loginTimeMilis),Long.toString(logoutTimeMilis),Long.toString(activateTime));
            db.close();
        }catch (Exception ex){
            PropClass.log(Level.SEVERE, ex.getMessage());
            return false;
        }
        return true;
    }
    public static String getStatistics(User user){
        Jedis db = new Jedis(HOST);
        long listLen=db.llen(user.getUsername());
        List<String> listData=db.lrange(user.getUsername(),2,listLen);
        StringBuilder statistics=new StringBuilder();
        SimpleDateFormat formater=new SimpleDateFormat("HH:mm:ss dd.MM.yyyy.");
        for(int i=0;i<listLen-2;i++)
        {
            if(i%3 == 0)
                 statistics.append("Login date and time: " + formater.format(new Date(Long.parseLong(listData.get(i)))));
            else if(i%3==1)
                statistics.append(", logout date and time: " + formater.format(new Date(Long.parseLong(listData.get(i))))+", ");
            else statistics.append("activated time: "+listData.get(i)+"secounds" +"\n");
        }
        db.close();
        return statistics.toString();
    }
    public static boolean changePassword(User user){
         try {
             Jedis db = new Jedis(HOST);
             db.lset(user.getUsername(), 0, user.getPassword());
             db.close();
         }catch (Exception ex){
             PropClass.log(Level.SEVERE, ex.getMessage());
             return false;
         }
         return true;
    }
    public static String[] getAllUsers(){
        try {
            Jedis db = new Jedis(HOST);
            Set<String> set=db.keys("*");
            String[] array = set.toArray(String[]::new);
            db.close();
            return array;
        }catch (Exception ex){
            PropClass.log(Level.SEVERE, ex.getMessage());
           return null;
        }

    }


}

